
let weather = {
  apiKey: "290e8cfe168ff682377bb20402a4eb05",
  fetchWeather: function (city, state) {
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city},${state}&units=metric&lang=pt_br&appid=${this.apiKey}&cnt=3`)
      .then((Response) => Response.json())
      .then((data) => this.displayWeather(data));
  },
  displayWeather: function (data) {
    const { name } = data;
    const { icon, description } = data.weather[0];
    const { temp, humidity } = data.main;
    const { speed } = data.wind;
    console.log(name, icon, description, temp, humidity, speed);
    document.querySelector(".city").innerText = "Clima em " + name;
    document.querySelector(".icon").src = `https://openweathermap.org/img/wn/${icon}.png`;
    document.querySelector(".description").innerHTML = description;
    document.querySelector(".temp").innerText = temp + "°C";
    document.querySelector(".humidade").innerText = "Humidade: " + humidity + "%";
    document.querySelector(".wind").innerText = "Vento: " + speed + "km/h";
    document.querySelector(".weather").classList.remove("loading");
    document.body.style.backgroundImage = `url(https://source.unsplash.com/1920x1080?${name})`
  },
  search: function () {
    this.fetchWeather(document.querySelector(".search-bar").value);
  },
};

document.querySelector(".search button").addEventListener("click", function () {
  weather.search();
});

document.querySelector(".search-bar").addEventListener("keyup", function (event) {
  if (event.key == "Enter") {
    weather.search();
  }
});

weather.fetchWeather("São Paulo");

